import cv2
import numpy as np
cam=cv2.VideoCapture(0);
faceDetect=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
rec=cv2.face.createLBPHFaceRecognizer();
rec.load('recognizer/trainningData.yml')
Id = 1
font= cv2.FONT_HERSHEY_SIMPLEX
while(True):
	ret,img=cam.read();
	gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	faces=faceDetect.detectMultiScale(gray,1.1,4);
	for(x,y,w,h) in faces:
		#cv2.ellipse(img,(256,256),(100,50),0,0,180,255,-1)
		cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
		#Id,conf=rec.predict(gray[y:y+h,x:x+w])
		if(Id == 1):
			Id = "Thanh Soi"
		elif(Id == 2):
			Id = "Obama"
		cv2.putText(img,str(Id),(200,350), font, 0.5,(255,255,255),2,cv2.LINE_4)

	cv2.imshow('Face',img);
	if(cv2.waitKey(1)==ord('q')):
		break;
cam.release()
cv2.destroyAllWindows()